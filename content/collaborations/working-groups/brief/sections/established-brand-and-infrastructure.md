---
title: An Established Brand and Infrastructure Increase Ecosystem Health, Visibility
section_id: established-brand-and-infrastructure
weight: 4
---

The growing number of siloed, special-purpose, and single-topic open source associations and foundations fractures and weakens the global open source ecosystem.

With so much noise in the open source universe, it’s difficult for associations, particularly smaller ones, to create the visibility needed for sustainable
operations. Staffing and financial shortfalls, and lack of experience nurturing open source communities, compound the challenges. And every association is
chasing a finite member pool.

The Eclipse Foundation brand is globally recognized and proven to boost credibility for ecosystems and their members. With consistent governance, licensing, and life
cycle models across all communities and projects, it’s easy and cost-effective for organisations to participate in all initiatives that interest them.

To support and nurture the working groups it hosts, the Eclipse Foundation provides the services many ecosystems are unable to deliver on their own, including:

- IP and trademark management and licensing
- Ecosystem development and marketing
- Collaborative management
- Specification development
- Branding and compatibility
- Marketing and promotion
- IT infrastructure

With no need to worry about staffing and delivering these critical services, ecosystems are free to focus on technology and innovation objectives while continuing to
grow and diversify.
