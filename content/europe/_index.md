---
title: "Europe"
date: 2022-07-19T16:05:57-04:00
description: "The Eclipse Foundation has transitioned to Europe as part of our continued global expansion. Our goal is to grow the global ecosystem and advance strategic open source projects with international impact."
categories: []
keywords: ["eclipse", "europe"]
slug: ""
seo_title: Enabling Global Collaboration on Open Source Innovation
aliases: []
toc: false
draft: false
header_wrapper_class: "header-europe-bg-img"
#seo_title: ""
headline: "Enabling Global Collaboration on Open Source Innovation"
#subtitle: ""
#tagline: ""
links:
  - - href: /collaborations/
    - text: Industry Collaborations
    - class: "btn btn-white"
  - - href: /membership/
    - text: Members
    - class: "btn btn-white"
  - - href: /org/value/
    - text: Industry Value
    - class: "btn btn-white"
page_css_file : public/css/europe-styles.css
hide_sidebar: true
hide_page_title: true
container: container-fluid
layout: single
---

{{< pages/europe/landing-page >}}
