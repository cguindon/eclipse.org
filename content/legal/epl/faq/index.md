---
title: Eclipse Public License 1.0 (EPL) Frequently Asked Questions
author: Mike Milinkovich
keywords:
  - epl
  - cpl
  - faq
  - legal
  - foundation
  - eclipse
  - license
  - licenses
related:
  links:
    - text: Eclipse Legal FAQ
      url: /legal/faq
    - text: Eclipse Software User Agreement
      url: /legal/epl/notice
---

{{< pages/legal/epl-faq >}}
