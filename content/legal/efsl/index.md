---
title: Eclipse Foundation Specification License
author: Eclipse Foundation
keywords:
    - legal
    - Eclipse Foundation Specification License
---

**Do not modify the text of this license.**

Specification project committers: include this text _verbatim_ in your specification document.

An AsciiDoc version of this file is available [here](/legal/adoc/efsl-2.0.adoc). Plain HTML is [here](efsl.html).

The Eclipse Foundation Specification License version 1.1 is available [here](/legal/efsl-1.1).

The Eclipse Foundation TCK License is [here](/legal/tck).

&darr; The actual licence text starts below this line. &darr;

***

{{< pages/legal/inject-content >}}
