---
title: "Approved Licenses for Non-Code Content"
author: Mike Milinkovich
keywords:
    - legal
    - guide
    - summary
---

This document lists the licenses approved by the Board of Directors for use for Non-Code Content
(as that term is defined in the [Eclipse.org Terms of Use](/legal/terms-of-use)). They are:

- Eclipse Public License 1.0
