---
title: "Committer Due Diligence Guidelines"
author: Mike Milinkovich
hide_page_title: true
keywords:
    - legal
    - foundation
    - committer
    - committers
    - diligence
    - guidelines
    - IP
    - intellectual property
    - policy
    - procedure
---

{{< pages/legal/inject-content >}}
