---
title: Edge & IoT 
description: >- 
  We enable industry leaders to collaborate on an end-to-end IoT architecture
  that is secure, flexible, and fully based on open source and open standards.
header_wrapper_class: header-topic
jumbotron_container: container-fluid
jumbotron_class: " "
custom_jumbotron: |
  <div class="custom-jumbotron-main container">
    <h1 class="featured-jumbotron-headline">Edge & IoT</h1>
    <p>
      We enable industry leaders to collaborate on an end-to-end IoT
      architecture that is secure, flexible, and fully based on open source and
      open standards.
    </p>
  </div>
custom_jumbotron_class: col-md-24
hide_jumbotron: false
show_featured_story: true
hide_breadcrumb: true
hide_sidebar: true
hide_page_title: true
page_css_file: /public/css/topics.css
---

{{< grid/div class="padding-top-60 padding-bottom-40" isMarkdown="false" >}}
    <h2 class="margin-bottom-40" id="projects">Projects</h2>
    <div 
      class="eclipsefdn-topic-projects display-flex margin-top-40" 
      data-technology-types="iot%20and%20edge" 
    >
    </div>

{{</ grid/div >}}
{{< mustache_js template-id="tpl-project-basic-card" path="/js/src/templates/tpl-project-basic-card.mustache" >}}

{{< pages/topics/communities topic="edge_and_iot" >}}

{{< grid/div class="padding-top-60 padding-bottom-40" isMarkdown="false" >}}
    <h2 class="margin-bottom-40" id="insights-and-resources">Insights & Resources</h2>
    {{< newsroom/resources wg="sparkplug, edge_native, eclipse_iot, oniro" type="case_study, white_paper, market_report, social_media_kit" template="cover" limit="3" >}}
{{</ grid/div >}}

