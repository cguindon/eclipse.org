/*!
 * Copyright (c) 2021, 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Christopher Guindon <chris.guindon@eclipse-foundation.org>
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

require('./node_modules/eclipsefdn-solstice-assets/webpack-solstice-assets.mix.js');
let mix = require('laravel-mix');
mix.EclipseFdnSolsticeAssets();

mix.setPublicPath('static/public');
mix.setResourceRoot('../');

// Public JS Bundles (DO NOT MODIFY)
mix.js('js/common/cookie-consent.js', './static/public/js/cookie-consent.js');
mix.js('js/common/eclipsefdn.videos.js', './static/public/js/eclipsefdn.videos.js');

mix.js('js/common/quicksilver/main.js', './static/public/js/quicksilver/main.js');
mix.js('js/common/quicksilver/barebone.js', './static/public/js/quicksilver/barebone.js');

mix.js('js/common/astro/main.js', './static/public/js/astro/main.js');
mix.js('js/common/astro/barebone.js', './static/public/js/astro/barebone.js');

mix.js('node_modules/cookieconsent/build/cookieconsent.min.js', './static/public/js/common/vendor/cookieconsent.js');

// Public CSS Bundles (DO NOT MODIFY)
// Eclipse IDE
mix.less(
  'node_modules/eclipsefdn-solstice-assets/less/quicksilver/eclipse-ide/styles.less',
  'static/public/css/eclipse-ide.css'
);
// Quicksilver
mix.less(
  'node_modules/eclipsefdn-solstice-assets/less/quicksilver/styles.less',
  'static/public/css/quicksilver.css'
);
// Astro
mix.less(
  'node_modules/eclipsefdn-solstice-assets/less/astro/main.less',
  'static/public/css/astro.css'
);
// Jakarta EE
mix.less(
  'node_modules/eclipsefdn-solstice-assets/less/quicksilver/jakarta/styles.less',
  'static/public/css/jakarta.css'
);
// Barebone
mix.less(
  'node_modules/eclipsefdn-solstice-assets/less/solstice/_barebone/styles.less',
  'static/public/css/barebone.css'
);
// Barebone Footer
mix.less(
  'node_modules/eclipsefdn-solstice-assets/less/solstice/_barebone/footer.less',
  'static/public/css/barebone-footer.css'
);
// Videos
mix.less(
  'node_modules/eclipsefdn-solstice-assets/less/_components/eclipsefdn-video.less',
  'static/public/css/eclipsefdn-video.css'
);
// Table
mix.less(
  'node_modules/eclipsefdn-solstice-assets/less/solstice/table.less',
  'static/public/css/table.css'
);
// Cookie Consent
mix.copy(
  'node_modules/cookieconsent/build/cookieconsent.min.css',
  'static/public/css/vendor/cookieconsent/cookieconsent.css'
);

// Below was the file previously named "forums.min.css". This line will serve
// as a reminder that this is a file we'll need to build once
// eclipse.org-common is retired.
// mix.less('node_modules/eclipsefdn-solstice-assets/less/solstice/forums.less', 'static/public/css/common/forums.css');

// Below was the file previously named "styles.min.css". This line will serve
// as a reminder that this is a file we'll need to build once
// eclipse.org-common is retired.
// mix.less('node_modules/eclipsefdn-solstice-assets/less/solstice/styles.less', 'static/public/css/common/styles.css');


// Default styles for eclipse.org (only used by this site)
mix.less('./less/styles.less', 'static/public/css/styles.css');
mix.js('js/main.js', './static/public/js/main.js');

// pages
mix.less('./less/pages/homepage.less', 'static/public/css/homepage.css');
mix.less('./less/pages/collaborations/styles.less', 'static/public/css/collaborations-styles.css');
mix.less('./less/pages/openchain/styles.less', 'static/public/css/projects-openchain.css');
mix.less('./less/pages/europe/styles.less', 'static/public/css/europe-styles.css');
mix.less('./less/pages/org/governance.less', 'static/public/css/org/governance.css');
mix.less('./less/pages/org/value.less', 'static/public/css/org/value.css');
mix.less('./less/pages/projects/resources.less', 'static/public/css/projects/resources.css');
mix.less('./less/pages/sponsor/collaboration.less', 'static/public/css/collaboration.css');
mix.less('./less/pages/blogs-and-videos.less', 'static/public/css/blogs-and-videos.css');
mix.less('./less/pages/topics.less', 'static/public/css/topics.css');
mix.less('./less/pages/resources/marketplaces.less', 'static/public/css/resources/marketplaces.css');
mix.less('./less/pages/careers.less', 'static/public/css/careers.css');
mix.less('./less/pages/membership/prospectus.less', 'static/public/css/membership-prospectus.css');
mix.less('./less/pages/research/styles.less', 'static/public/css/research-styles.css');
mix.less('./less/pages/specifications/styles.less', 'static/public/css/specifications-styles.css');
mix.less('./less/pages/org/artwork.less', 'static/public/css/org/artwork.css');
mix.less('./less/pages/legal/about-file-templates.less', 'static/public/css/legal/about-file-templates.css');

// eclipse.org/security/known
mix.js('js/known-vulnerabilities.js', './static/public/js/known-vulnerabilities.js')
mix.css('./node_modules/datatables.net-dt/css/jquery.dataTables.min.css', 'static/public/css/datatables.net-dt.css');

// eclipse.org/articles
mix.js('js/articles.js', './static/public/js/articles.js')

mix.js('js/src/legal/generate-body-content.js', './static/public/js/generate-body-content.js')
mix.js('js/src/legal/generate-license-list.js', './static/public/js/generate-license-list.js')
mix.js('js/src/legal/generate-trademark-list.js', './static/public/js/generate-trademark-list.js')
mix.js('js/src/legal/generate-epl-about-content.js', './static/public/js/generate-epl-about-content.js')
