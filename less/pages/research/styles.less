/*!
 * Copyright (c) 2019, 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Christopher Guindon <chris.guindon@eclipse-foundation.org>
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
*/

@import (reference) '~eclipsefdn-solstice-assets/less/astro/main.less';
@import '_variables.less';

@import url('https://fonts.googleapis.com/css2?family=Inconsolata:wght@200..900&display=swap');

//== Utilities

// Colors
.text-research-primary {
  color: @palatinate-blue;
}

.text-greenish-cyan {
  color: @greenish-cyan;
}

// Font Families
.text-research-monospace {
  font-family: 'Inconsolata', monospace;
}

// Font Size
.text-lg {
  font-size: @font-size-lg;
}

.text-xl {
  font-size: @font-size-xl;
}

// Links
.inherit-link-colors {
  a {
    &,
    &:link,
    &:hover,
    &:active {
      color: inherit;

      &:not(.btn) {
        text-decoration: underline;
      }
    }
  }
}

.links-hover-primary {
  a {
    &:hover,
    &:active {
      color: @brand-primary;
    }
  }
}

// Backgrounds
.bg-white {
  background: #fff;
}

.radial-bg {
  position: relative;

  &::before {
    position: absolute;
    content: '';
    display: block;
    height: 100%;
    left: 50%;
    transform: translateX(-50%);
    aspect-ratio: 1;
    border-radius: 50%;
    background-color: fade(@brand-research-primary, 10%);
    filter: blur(30px);
    z-index: -1;
  }
}

.bg-research-connected-gradient {
  background: linear-gradient(to right, @brand-research-primary 0%, #170F72 100%);
}

.bg-research-gradient {
  position: relative;

  &::before {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background: linear-gradient(0deg, rgba(169,223,255,1) 0%, rgba(255,255,255,0) 100%);
  }
}

.bg-wire-waves {
  position: relative;
  overflow: hidden; // Required for Safari

  &::before {
    background-color: #020962;
  }

  &::after {
    background: url('../../images/research/wire-wave.svg');
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center;
  }

  &::before,
  &::after {
    content: '';
    width: 100%;
    min-height: 100%;
    // Safari can't calculate 100% height, so use webkit-fill-available instead
    min-height: -webkit-fill-available; 
    top: 0;
    left: 0;
    position: absolute;
    z-index: -1;
  }
}

//== Base

// Scrolling Behavior
html,
body {
  scroll-behavior: smooth;

  @media (prefers-reduced-motion: reduce) {
    scroll-behavior: auto;
  }
}

// Typography
h1,
.h1,
h2,
.h2 {
  letter-spacing: 0;
}

h2,
.h2 {
  font-size: 2.8rem;
  font-weight: 700;
  color: black;

  @media (min-width: @screen-sm-min) {
    font-size: @font-size-h2;
  }
}

.featured-section {
  font-size: 1.6rem;

  @media (min-width: @screen-sm-min) {
    font-size: 2rem;
  }
}

//== Layout

.header-wrapper {
  background-color: #000;
}

// Jumbotron
.featured-jumbotron {
  position: relative;
  background: url('../../images/research/research-bg.jpg');
  background-repeat: no-repeat;
  background-size: cover;
  padding-bottom: 0;
  overflow: hidden;

  &::before {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background: fade(#000966, 63);
  }
  
  &,
  & p {
    font-size: 2rem;
  }

  & &-headline {
    font-family: 'Inconsolata', monospace;
    color: #fff;
    font-weight: 500;
    font-size: 5rem;

    @media (min-width: @screen-sm-min) {
      font-size: 8rem;
    }
  }

  .text-highlighted {
    color: @greenish-cyan;
  }
}

.research-jumbotron-bottom-area {
  background-color: #00000033;
  padding: 1rem 0;
  backdrop-filter: blur(5px);

  @media (min-width: @screen-sm-min) {
    padding: 2rem 0;
  }

  &,
  p {
    font-family: 'Inconsolata', monospace;
    font-size: @font-size-xl;
    font-weight: 700;

    @media (min-width: @screen-sm-min) {
      font-size: 3rem;
    }
  }

  p {
    margin: 0;
    margin-left: 15px;
    margin-right: 15px;
  }
}
.research-jumbotron-ctas {
  display: flex;
  align-items: center;
}

.research-jumbotron-stats {
  display: flex;
  width: 100%;
  flex-wrap: wrap;
  justify-content: space-between;
  gap: 1rem;
  backdrop-filter: blur(3px);

  @media (min-width: @screen-sm-min) {
    flex-wrap: nowrap;
  }

  & > * {
    width: calc(50% - 1rem);

    @media (min-width: @screen-sm-min) {
      width: 100%;
    }
  }

  & .stats-item {
    border-radius: 1rem;
    border: 2px solid @palatinate-blue; 
    padding: 1.5rem;

    .stat {
      display: block;
      color: #fff;
      font-size: 3.6rem;
      font-weight: 700;
    }

    .stat-name {
      display: block;
      color: @greenish-cyan;
      font-size: 1.4rem;
      text-transform: uppercase;
    }
  }
}

.research-animated-text {
  overflow: hidden;
  display: inline-block;
  border-right: .15em solid @greenish-cyan;
  padding-right: .3em;
  white-space: nowrap;
  margin: 0 auto;
  max-width: fit-content;
  text-align: left;
  animation:
    typing 6s steps(20, end) alternate infinite,
    blink-cursor 1s step-end infinite;

  @media (min-width: @screen-sm-min) {
    // Animation steps need to be tweaked depending on screen size. More steps
    // are required for larger screens.
    animation:
      typing 6s steps(70, end) alternate infinite,
      blink-cursor 1s step-end infinite;
  }
}


//== Components

// About Card
.about-card {
  padding: 4rem 6rem;
  border-radius: 1rem;
  background-color: white;
  box-shadow: 5px 10px 20px fade(#000966, 20%);
  margin-top: 8rem;
  margin-bottom: 8rem;
  
  .eclipsefdn-video {
    filter: drop-shadow(0 10px 20px #00096688);
  }
}

// Research Card List
.research-card-list {
  li {
    .text-research-monospace;

    display: flex;
    align-items: center;
    color: #fff;
    background-color: #000966;
    border-radius: 1rem;
    padding: 2.5rem;
    margin-bottom: 2rem;
    gap: 2rem;
    font-weight: 300;
    border: 1px solid #54FFD4;
  }
}

// Research Checklist
.research-checklist {
  padding-inline-start: 25px;

  li {
    margin-bottom: 2rem;
  }

  li::marker {
    content: '\f111' ' ';
    font-family: 'Font Awesome 6 Free';
    font-weight: 700;
  }
  .checked::marker {
    content: '\f058' ' '; // Adds a space between the icon and the text
    color: @brand-research-primary;
  }
}

// Funding Logos
.funding-logos {
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;

  img {
    width: 100%;
    flex-shrink: 0;

    @media (min-width: @screen-sm-min) {
      width: 50%;
    }
  }
}

// Research Team Member
.research-team-member {
  padding: 2rem;
  text-align: center;

  img {
    width: 100%;
    aspect-ratio: 1;
    object-fit: cover;
    object-position: top;
    margin-bottom: 1rem;
    border-radius: 2rem;
  }
  
  &-name {
    color: black;
    font-size: 1.9rem;
    font-weight: 500;
  }
  &-title {
    font-family: 'Inconsolata', monospace;
    font-size: @font-size-lg;
  }
}

// Research Team Info Block
.research-team-info-block {
  display: flex;
  width: calc(100% - 4rem);
  aspect-ratio: 1;
  background-color: blue;
  margin: 2rem;
  align-items: center;
  justify-content: center;
  text-align: center;
  border-radius: 2rem;
  background-color: #E2F4FF;
  color: #000966;
  font-family: 'Inconsolata', monospace;
  font-weight: 600;
  font-size: @font-size-xl;
}

// Contact Form
.research-contact-form {
  padding: 4rem;
  border-radius: 1.5rem;
  background-color: #fff;
}

// Newsroom Resources
.newsroom-resources {
  display: flex;
  gap: 2rem;
  justify-content: flex-start;
  padding-bottom: 3rem;
  overflow-x: auto;
  overflow-y: hidden;
  filter: drop-shadow(10px 10px 10px #a7a2a229);

  & > * {
    width: 100%;
    flex-shrink: 0;

    @media (min-width: @screen-sm-min) {
      width: calc(25% - 2rem);
    }

    @media (min-width: @screen-lg-min) {
      width: calc(25% - 2rem);
    }
  }
}

// Newsroom Resource Card
.newsroom-resource-card {
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  border: 1px solid #e6e6e6;
  //box-shadow: 10px 10px 10px #a7a2a229;
  padding: 0;
  background-color: #eee;
  border-radius: 1.5rem;
  overflow: hidden;
  max-width: 28rem;

  .image-container {
    height: 100%;
  }

  img {
    width: 100%;
    height: 100%;
    object-fit: cover;
  }

  .success-stories-download {
    .btn-lg;
  }
}

// Featured Projects
.featured-projects {
  position: relative;
  padding: 0;
  font-size: @font-size-md;

  &::before,
  &::after {
    content: '';
    display: inline-block;
    position: absolute;
    top: 0;
    width: 1rem;
    height: calc(100% - 4rem);
    z-index: 2;
  }

  &::before {
    left: 0;
    background: linear-gradient(to left, transparent 0%, white 100%);
  }

  &::after {
    right: 0;
    background: linear-gradient(to right, transparent 0%, white 100%);
  }

  & &-list.research-projects-row {
    flex-wrap: nowrap;
    overflow-x: scroll;
    overflow-y: auto;
    padding-left: 1rem;
    padding-right: 1rem;
    padding-bottom: 4rem;
  }

  & > div {
    display: none;
  }

  // Featured Projects Item
  &-item {
    display: flex;
    flex-direction: column;
    border-radius: 1.5rem;
    box-shadow: 0 4px 11px rgba(0, 0, 0, 0.25);

    &-content .link {
      max-width: 75%;
    }

    &-category {
      background-color: #000966;
    }

    .details {
      display: flex;
      flex-direction: column;
      height: 100%;
      justify-content: space-between;
    }

    hr {
      border-color: #000966;
    }
  }

  @media (min-width: @screen-lg-min) {
    &-list-cards .featured-project {
      flex-basis: calc(25% - 2rem);
    }
  }

  &.hide-view-all .view-all-card {
    display: none;
  }

  .view-all-card {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    gap: 2rem;
    min-width: 26rem;
    color: @brand-research-primary;
    background-color: #E2F4FF; 
    font-size: 2rem;

    i {
      font-size: 4rem;
    }
  }
}

.featured-projects .featured-projects-list-cards .featured-project {
  @media (min-width: @screen-lg-min) {
    flex-shrink: 0;
    flex-basis: calc(25% - 2rem);
  }
}

// Accordion
.panel-research {
  background-color: #fff;
  border: unset;
  box-shadow: 0 4px 11px fade(black, 25%);

  // Icon
  .panel-heading img {
    margin-right: 1rem;
  }

  .panel-heading {
    padding: 0;
  }

  .panel-heading a,
  .panel-body {
    padding: 2rem;
  }

  .panel-body {
    padding-top: 0;
    font-size: @font-size-md;

    @media (min-width: @screen-sm-min) {
      font-size: @font-size-lg;
    }
  }

  .panel-title {
    font-weight: 500;

    @media (min-width: @screen-sm-min) {
      font-size: 2rem;
    }
    
    // The accordion button
    a {
      display: flex;
      align-items: center;
      justify-content: space-between;

      &:hover,
      &:focus,
      &:active {
        text-decoration: none;
      }
    }

    a[aria-expanded='true'] {
      .open-icon {
        display: none;
      }

      .close-icon {
        display: inline;
      }
    }

    a[aria-expanded='false'] {
       .open-icon {
        display: inline;
      }

      .close-icon {
        display: none;
      }
    }
  }

  .fa-stack {
    font-size: 1.5em;
  }
  .open-icon,
  .close-icon {
    color: @brand-research-primary;
  }
  .icon-bg {
    color: @pattens-blue;
  }

  .panel-heading + .panel-collapse > .panel-body {
    border: none;
  }
}

.panel-group .panel-research {
  border-radius: 2.4rem;
}

.panel-group .panel-research + .panel-research {
  margin-top: 1rem;
}


//== Page Sections

// Wire Wave Graphic
.wire-waves-graphic {
  position: relative;
  max-width: 200rem;

  @media (min-width: @screen-lg-min) {
    margin: auto;
  }

  &::after {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    @media (min-width: @screen-lg-min) {
      background: linear-gradient(to right, #fff 0%, transparent 10%, transparent 90%, #fff 100%); 
    }
  }

  .wire-waves {
    width: 100%;
    height: auto;
    max-width: 200rem;
  }

  .container {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translateX(-50%) translateY(-75%);

    // Bootstrap 3 uses width:auto; for containers which are smaller than their
    // max-width. This is incompatible with position:absolute; so we need to
    // override it with something that works.
    @media (max-width: @screen-lg-min) {
      min-width: 100%;
    }
  }

  .foreground {
    margin-top: 6rem;
    max-width: 100%;
  }
}


// Funding Section
.funding-section {
  @media (min-width: @screen-sm-min) {
    padding-bottom: 24rem;
  }

  img {
    max-width: 24rem;
    max-height: 14rem;
    object-fit: contain;
    margin: auto;
  }
}

// What We Offer Section
.what-we-offer-section .radial-bg::before {
  height: 110%;
  transform: translateX(-50%) translateY(-10%);
}

// Groups We Support Section
.groups-we-support {
  position: relative;
  padding: 4rem;
  border-radius: 2rem;
  background: radial-gradient(circle, rgba(120, 119, 232, 40%) 0%, transparent 60%);
  text-align: center;
  overflow: hidden;

  &::before {
    position: absolute;
    content: '';
    z-index: -1;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    background-color: @pattens-blue;
  }
  
  ul {
    display: flex;
    flex-wrap: wrap;
    gap: 1rem;
  }

  li {
    background: #fff;
    box-shadow: 0 4px 11px #00000033;
    border-radius: 1rem;
    padding: 4rem;
    width: 100%;
    flex-grow: 0;

    @media (min-width: @screen-sm-min) {
      width: calc(25% - 1rem);
    }

    img {
      width: 100%;
      max-height: 6rem;
      height: 100%;
      object-fit: contain;
    }
  }
}

// Stay Connected Section
.stay-connected-section {
  // Set the color of fa-inverse to brand-research-primary
  --fa-inverse: @brand-research-primary;
}

//== Animations

// Typing Animation
@keyframes typing {
  from { width: 0 }
  to { width: 100% }
}

// Blinking Cursor Animation
@keyframes blink-cursor {
  from, to { border-color: transparent }
  50% { border-color: @greenish-cyan }
}

//== Hotfixes
.featured-projects .search {
  display: none; 
}

