#!/bin/bash

#!
# Copyright (c) 2024 Eclipse Foundation, Inc.
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License v. 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0

# The location of the directory where we used to host asset files on
# eclipse.org-common.
LEGACY_BUNDLE_LOCATION="https://www.eclipse.org/eclipse.org-common/themes/solstice/public"

# Downloads a CSS asset file and places it in the specified location. Handles
# errors if the file could not be downloaded.
function getCssAssetFile() {
  src=$1 # The URL where the asset file is coming from
  dest=$2 # The destination of where to download it.
  
  # Create the directories of the given asset file's destination
  mkdir -p "$(dirname "$dest")"

  # Retrieve the Content-Type header of the asset file. 
  #
  # This is to ensure it exists on eclipse.org-common since we can't trust the
  # HTTP status codes to tell us if the file has been found. This is because
  # even the "Not Found" pages return a 200.
  #
  # The Content-Type's value is in the second column, so we use cut.
  content_type=$(curl -sI "$src" | grep -i "Content-Type" | cut -d' ' -f2)

  # If the response's content type does not contain "text/css", exit the
  # process. 
  if [[ "$content_type" != *"text/css"* ]]; then
    echo "CSS asset file not found."
    echo "Exiting"
    exit 1
  fi

  # Download the asset file and output it to the destination
  curl -s "$src" > "$dest"

  # Exit the postinstall process with an error code if asset file could not
  # download.
  if [ $? -ne 0 ]; then
    echo "Could not download ${src}"
    echo "Exiting"
    exit 1
  else
    echo "Successfully downloaded ${src}"
  fi
}

# Fetch Projects
# Outputs a YAML data file containing Eclipse projects.
echo "Generating projects data file"
NODE_ENV=production node node_modules/eclipsefdn-hugo-solstice-theme/bin/projects/index.js -l data/

# Fetch Blogs
# Downloads the blog feed and creates an XML data file.
echo "Downloading blogs feed as data file"
curl -s "https://blogs.eclipse.org/blog/feed" > "./data/staff_blogs.xml"

# Fetch CSS files
echo "Downloading base Solstice stylesheet"
getCssAssetFile "${LEGACY_BUNDLE_LOCATION}/stylesheets/styles.min.css" "./static/public/css/solstice.css"

echo "Downloading forums stylesheet"
getCssAssetFile "${LEGACY_BUNDLE_LOCATION}/stylesheets/forums.min.css" "./static/public/css/forums.css"

echo "Downloading barebone toolbar stylesheet"
getCssAssetFile "${LEGACY_BUNDLE_LOCATION}/stylesheets/barebone-toolbar.min.css" "./static/public/css/barebone-toolbar.css"

echo "Post-installation done"

