<!--
  Copyright (c) 2024 Eclipse Foundation, Inc.

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License v. 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
-->
<p>Last updated March 22, 2022</p>
<p>
  The <a href="/legal/eca">Eclipse Contributor Agreement</a> (ECA) is a key
  document in the process of managing intellectual property contributions to
  Eclipse projects. This FAQ is intended to help explain what they are, and
  how they are used.
</p>
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  <div class="panel panel-info">
    <div class="panel-heading" role="tab" id="headingOne">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true"
          aria-controls="collapseOne"> 1. What does the ECA do? </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">The purpose of the ECA is to provide a written
        record that you have agreed to provide your contributions of code and
        documentation under the licenses used by the Eclipse project(s) you're
        contributing to. It also makes it clear that you are promising that
        what you are contributing to Eclipse is code that you wrote, and you
        have the necessary rights to contribute it to our projects. And
        finally, it documents a commitment from you that your open source
        contributions will be permanently on the public record.</div>
    </div>
  </div>
  <div class="panel panel-info">
    <div class="panel-heading" role="tab" id="headingTwo">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"
          aria-expanded="false" aria-controls="collapseTwo"> 2. Why is a ECA necessary? </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
      <div class="panel-body">It's basically about documenting the provenance
        of all of the intellectual property coming into Eclipse. We want to
        have a clear record that you have agreed to the terms under which the
        Eclipse community has agreed to accept contributions.</div>
    </div>
  </div>
  <div class="panel panel-info">
    <div class="panel-heading" role="tab" id="headingThree">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree"
          aria-expanded="false" aria-controls="collapseThree"> 3. Does this means that the Eclipse Foundation will own
          my code? </a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">No. The ECA does not include a license or
        assignment to the Eclipse Foundation. Unlike many other open source
        communities or projects, Eclipse simply wants you to license your
        contributions under the open source license(s) used by the project. So
        you (or your employer) will continue to own your code.</div>
    </div>
  </div>
  <div class="panel panel-info">
    <div class="panel-heading" role="tab" id="headingFour">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour"
          aria-expanded="false" aria-controls="collapseFour"> 4. Why do you need to know my personal information? </a>
      </h4>
    </div>
    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
      <div class="panel-body">As part of our open source project
        recordkeeping, we want to be able to link every line of code to the
        people who contributed them. In the unlikely event of a dispute over
        authorship or copyright provenance, Eclipse needs to be able to
        demonstrate when the code arrived at Eclipse, how we acquired the
        rights to that code, and who wrote it</div>
    </div>
  </div>
  <div class="panel panel-info">
    <div class="panel-heading" role="tab" id="headingFive">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive"
          aria-expanded="false" aria-controls="collapseFive"> 5. How long is the ECA good for? </a>
      </h4>
    </div>
    <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
      <div class="panel-body">The ECA is good for three years, after which it
        will be expired and you will be asked to complete a new one. However,
        if your personal information changes (e.g. your address or your
        employer), we ask that you invalidate your current ECA and complete a
        new one.</div>
    </div>
  </div>
  <div class="panel panel-info">
    <div class="panel-heading" role="tab" id="headingSix">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix"
          aria-expanded="false" aria-controls="collapseSix"> 6. Does the ECA replace my committer agreement? </a>
      </h4>
    </div>
    <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
      <div class="panel-body">No. Your Committer Agreement is the agreement
        that allows you to have write access to the Eclipse Foundation source
        code repositories. The ECA is intended for people who are not
        committers who wish to make contributions to Eclipse projects.</div>
    </div>
  </div>
  <div class="panel panel-info">
    <div class="panel-heading" role="tab" id="headingSeven">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven"
          aria-expanded="false" aria-controls="collapseSeven"> 7. I am an Eclipse committer. Do I need to sign a ECA?
        </a>
      </h4>
    </div>
    <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
      <div class="panel-body">
        No. If you are a committer, then you are covered by either a Member Committer
        and Contributor Agreement (MCCA) or Individual Committer Agreement (ICA). Both
        of these agreements contain the ECA, so by virtue of being covered by one of
        those documents, you are automatically covered by the ECA.
      </div>
    </div>
  </div>
  <div class="panel panel-info">
    <div class="panel-heading" role="tab" id="headingEight">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEight"
          aria-expanded="false" aria-controls="collapseEight"> 8. What happens if I don't sign the ECA? </a>
      </h4>
    </div>
    <div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
      <div class="panel-body">Nothing, except you won't be allowed to
        contribute to open source projects at Eclipse</div>
    </div>
  </div>
  <div class="panel panel-info">
    <div class="panel-heading" role="tab" id="headingNine">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseNine"
          aria-expanded="false" aria-controls="collapseNine"> 9. Eclipse has been around a long time. Why are you
          doing ECAs now?
        </a>
      </h4>
    </div>
    <div id="collapseNine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingNine">
      <div class="panel-body">We want to make it easier for you to contribute
        to Eclipse projects. Previously, we have been asking contributors to
        agree to the equivalent of the ECA via Bugzilla or Gerrit on each and
        every contribution. Moving to ECAs is intended to streamline that
        process, and make it easier for contributions to be accepted by
        Eclipse projects.</div>
    </div>
  </div>
  <div class="panel panel-info">
    <div class="panel-heading" role="tab" id="headingTen">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTen"
          aria-expanded="false" aria-controls="collapseTen"> 10. How do I sign a ECA? </a>
      </h4>
    </div>
    <div id="collapseTen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTen">
      <div class="panel-body">
        Log into the <a href="/contribute/cla">Eclipse
          projects forge</a> (you will need to create an account with the
        Eclipse Foundation if you have not already done so); click on "Eclipse
        Contributor Agreement"; and Complete the form. Be sure to use the same
        email address when you register for the account that you intend to use
        on Git commit records.
      </div>
    </div>
  </div>
</div>